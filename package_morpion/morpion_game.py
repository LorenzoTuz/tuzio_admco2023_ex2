#!/usr/bin/env python
# coding: utf-8

"""
author : TUZIO Lorenzo

Ce code définit la classe morpion ( la grille et les comportements associé au jeu )
"""

class MorpionBoard:
    '''Création de la classe du plateau pour définir ce dernier
    ainsi que toute les méthode associées à cette classe'''
    def __init__(self):
        '''Définition de la taille du plateau ainsi que des conditions de victoires'''
        # Création du plateau de jeu carré 3x3 donc liste de 9 éléments.
        self.board=list(range(0,9))
        #print(self.board)
        # tuple qui contient respectivement les coins le centre et les autres cases
        self.moves=((1,7,3,9),(5,),(2,4,6,8))
        # Combbinaison où il faut placer son token pour remporter la victoire
        self.winners=((0,1,2),(3,4,5),(6,7,8),(0,3,6),(1,4,7),(2,5,8),(0,4,8),(2,4,6))

    def print_board(self):
        '''Méthode qui gère l'affichage du plateau de morpion'''
        caseboard=1
        for i in self.board:
            end = ' | ' # ajout d'une séparation de colonne

            if caseboard%3 == 0:   # création des lignes si on est dans la troisième colonne.
                end = ' \n'
                if i != 1:
                    end+='---------\n';
            char=' '
            #permet de décaler l'affichage si un charactère est olacé dans une case
            if i in ('X','O'):
                char=i;
            caseboard+=1
            print(char,end=end)

    def next_move(self,symbole,place):
        '''Méthode qui gère le prochain coup avec en argument le token à jouer et la case'''
        token = symbole
        case = place
        #test si le charactère choisi est valide
        if token != 'X' or token != 'O':
            raise ValueError("Le Token ne correspond pas à 'X' ou 'O'")
        #test si la case choisie est valide
        if case<0 or case>8:
            raise ValueError("Veuillez entrer une case comprise entre 0 et 8")
        # test si la case n'est pas déjà remplie
        if (self.board[case] == 'X') or (self.board[case] == 'O'):
            raise ValueError("La case est déja joué")
        self.board[case] = token
        for tuple in self.winners:
            case1,case2,case3=tuple
            # test si il ya un cas de victoire
            if (self.board[case1] == token) \
                and (self.board[case2] == token) \
                and (self.board[case3]==token):
                return True

jeu = MorpionBoard()
#jeu.board[0] = 'X'
# jeu.print_board()
# jeu.Next_move('X',2)
# jeu.print_board()
# jeu.Next_move('X',2)
# jeu.print_board()
# b=jeu.Next_move('X',0)
# print(b)
# b=jeu.Next_move('X',1)
# print(b)
# b=jeu.Next_move('X',2)
# print(b)
# jeu.print_board()
